package main;

public class Mesa {
	
	int pontos = 0;

	public void jogar() {
		Baralho baralho = new Baralho();
		System.out.println("a Mesa irá começar! primeira carta: ");

		while (pontos < 17) {

			Carta carta = baralho.sortear();
			int ponto = carta.getValor();

			System.out.println("Carta sorteada: " + carta + " que vale " + ponto + " pontos!");
			pontos += ponto;	
		}
		System.out.println("A mesa teve: " + pontos + " pontos!");
	}
}