package main;

public class Validador {
	
	public void validar(int pontosJogador, int pontosMesa) {
		
		if (pontosJogador > 21) {
			pontosJogador = 0;
		}
		if (pontosMesa > 21) {
			pontosMesa = 0;
		}
		
		if (pontosJogador > pontosMesa) {
			System.out.println("Você ganhou da mesa!");
		}
		else if (pontosMesa > pontosJogador) {
			System.out.println("A Mesa ganhou!");
		} else {
			System.out.println("Ninguem ganhou!");
		}
	}
}