package main;

public class App {
	public static void main(String[] args) {
		Baralho baralho = new Baralho();
		Mesa mesa = new Mesa();
		Jogador jogador = new Jogador();
		Validador validar = new Validador();
		
		jogador.jogar();	
		mesa.jogar();
		
		validar.validar(jogador.pontos, mesa.pontos);

	}
}
